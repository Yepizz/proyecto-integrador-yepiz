<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminViewSales extends Component
{
    public function render()
    {
        return view('livewire.admin-view-sales');
    }
}
