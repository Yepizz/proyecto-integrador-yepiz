<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AlmacenControl extends Component
{
    public function render()
    {
        return view('livewire.almacen-control');
    }
}
