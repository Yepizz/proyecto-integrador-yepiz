<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CrudCredits extends Component
{
    public function render()
    {
        return view('livewire.crud-credits');
    }
}
