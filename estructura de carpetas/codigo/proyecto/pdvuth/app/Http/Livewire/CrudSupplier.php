<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CrudSupplier extends Component
{
    public function render()
    {
        return view('livewire.crud-supplier');
    }
}
