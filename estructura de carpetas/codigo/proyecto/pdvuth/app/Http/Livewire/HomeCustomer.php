<?php

namespace App\Http\Livewire;

use Livewire\Component;

class HomeCustomer extends Component
{
    public function render()
    {
        return view('livewire.publico.home-customer');
    }
}
