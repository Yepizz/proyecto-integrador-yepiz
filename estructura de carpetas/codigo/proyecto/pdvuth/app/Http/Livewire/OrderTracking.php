<?php

namespace App\Http\Livewire;

use Livewire\Component;

class OrderTracking extends Component
{
    public function render()
    {
        return view('livewire.order-tracking');
    }
}
