<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductCrud extends Component
{
    public function render()
    {
        return view('livewire.product-crud');
    }
}
