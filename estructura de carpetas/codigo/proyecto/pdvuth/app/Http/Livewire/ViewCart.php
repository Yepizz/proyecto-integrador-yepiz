<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ViewCart extends Component
{
    public function render()
    {
        return view('livewire.view-cart');
    }
}
