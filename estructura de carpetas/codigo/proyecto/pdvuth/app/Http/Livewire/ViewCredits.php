<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ViewCredits extends Component
{
    public function render()
    {
        return view('livewire.view-credits');
    }
}
