<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ViewProductRegistered extends Component
{
    public function render()
    {
        return view('livewire.view-product-registered');
    }
}
