<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $table="clientes";

    protected $fillable=[
        'nombre','apellido_paterno','fecha_nacimiento','celular','correo',
        'metodo_pago','pais','region','contrasena','localidad','municipio',
        'codigo_postal','calle'
    ];
}
