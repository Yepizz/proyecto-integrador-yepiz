<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    use HasFactory;

    protected $table="creditos";

    protected $fillable=[
        'saldo','fecha_apertura','estado_credito','fecha_saldado'
    ];
}
