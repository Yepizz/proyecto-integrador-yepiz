<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $table="empresas";

    protected $fillable=[
        'nombre_empresa','num_identificacion','codigo_postal','telefono',
        'correo','calle','colonia','ciudad','estado','pais','municipio'
    ];


}
