<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    use HasFactory;

    protected $table="proveedores";

    protected $fillable=[
        'nombre','apellido_paterno','colonia','localidad','calle',
        'codigo_postal','celular','correo','activo','codigo_identificacion'

    ];
}
