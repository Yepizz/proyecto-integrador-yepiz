<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VentaDetalle extends Model
{
    use HasFactory;

    protected $table="ventas_detalles";

    protected $fillable=[
        'cantidad','precio','venta_id'
    ];
}
