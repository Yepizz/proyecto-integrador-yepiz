<?php

namespace Database\Factories;

use App\Models\ProveedorEmpresa;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProveedorEmpresaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProveedorEmpresa::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
