<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',60);
            $table->text('descripcion');
            $table->double('precio');
            $table->string('codigo_barras',13);
            $table->binary('imagenes');
            $table->enum('unidad_medida',['UNIDAD','KILO','CAJA','PAQUETE','LITRO']);
            $table->string('marca',40);
            $table->unsignedBigInteger('categoria_id');
            $table->integer('stock_minimo');
            $table->timestamps();

            $table->unsignedBigInteger('almacen_id_prod');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
