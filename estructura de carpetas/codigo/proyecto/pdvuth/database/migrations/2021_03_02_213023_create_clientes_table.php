<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',30);
            $table->string('apellido_paterno',30);
            $table->string('apellido_materno',30)->nullable();
            $table->date('fecha_nacimiento');
            $table->string('celular',10);
            $table->string('correo',30);
            $table->enum('metodo_pago',['PAYPAL','MERCADO_PAGO','TARJETA_CREDITO','EFECTIVO']);
            $table->string('pais',30);
            $table->string('region',30);
            $table->string('contrasena',30);
            $table->string('localidad',30);
            $table->string('municipio',30);
            $table->string('codigo_postal',5);
            $table->string('calle',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
