<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proveedor_id_empresa');
            $table->string('nombre',50);
            $table->string('apellido_paterno',30);
            $table->string('apellido_materno',30)->nullable();
            $table->string('colonia',30);
            $table->string('localidad',30);
            $table->string('calle',30);
            $table->string('codigo_postal',5);
            $table->string('celular',10);
            $table->string('correo',30);
            $table->boolean('activo')->default(TRUE);
            $table->string('codigo_identificacion',15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedors');
    }
}
