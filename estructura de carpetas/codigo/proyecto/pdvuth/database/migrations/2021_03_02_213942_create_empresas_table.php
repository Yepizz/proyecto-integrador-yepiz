<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empresa_id');
            $table->string('nombre_empresa',30);
            $table->integer('num_identificacion');
            $table->string('codigo_postal',5);
            $table->string('telefono',10);
            $table->string('correo',15);
            $table->string('calle',30);
            $table->string('colonia',30);
            $table->string('ciudad',30);
            $table->string('estado',30);
            $table->string('pais',20);
            $table->string('municipio',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
