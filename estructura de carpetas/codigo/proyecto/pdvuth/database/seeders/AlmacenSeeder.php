<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Almacen;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlmacenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $almacenes = [
            //fields here

            "1"=>[
                'id'=>1,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "2"=>[
                'id'=>2,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "3"=>[
                'id'=>3,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "4"=>[
                'id'=>4,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "5"=>[
                'id'=>5,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "6"=>[
                'id'=>6,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "7"=>[
                'id'=>7,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "8"=>[
                'id'=>8,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "9"=>[
                'id'=>9,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ],
            "10"=>[
                'id'=>10,
                'cantidad'=>5,
                'stock'=>50,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ]

        ];

        foreach($almacenes as $key=>$almacen){
            DB::table('almacens')->insert($almacen);
        }

    }
}
