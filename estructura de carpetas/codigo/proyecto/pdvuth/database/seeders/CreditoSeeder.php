<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreditoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $creditos=[
            "1"=>[
                'id'=>1,
                'cliente_id'=>1,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'SALDADO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "2"=>[
                'id'=>2,
                'cliente_id'=>2,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'CONGELADO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "3"=>[
                'id'=>3,
                'cliente_id'=>3,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'APERTURA',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ], "4"=>[
                'id'=>4,
                'cliente_id'=>4,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'APERTURA',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "5"=>[
                'id'=>5,
                'cliente_id'=>5,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'SALDADO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "6"=>[
                'id'=>6,
                'cliente_id'=>6,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'SALDADO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "7"=>[
                'id'=>7,
                'cliente_id'=>7,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'OPERANDO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "8"=>[
                'id'=>8,
                'cliente_id'=>8,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'SALDADO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "9"=>[
                'id'=>9,
                'cliente_id'=>9,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'CONGELADO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "10"=>[
                'id'=>10,
                'cliente_id'=>10,
                'saldo'=>456.00,
                'fecha_apertura'=>date('Y-m-d'),
                'estado_credito'=>'SALDADO',
                'fecha_saldado'=>date('Y-m-d'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
        ];

        foreach ($creditos as $key=>$credito){
            DB::table('creditos')->insert($credito);
        }
    }
}
