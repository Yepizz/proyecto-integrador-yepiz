<?php

namespace Database\Seeders;

use App\Models\Empresa;
use Illuminate\Database\Seeder;
use Database\Seeders\AlmacenSeeder;
use Database\Seeders\ClienteSeeder;
use Database\Seeders\CreditoSeeder;
use Database\Seeders\EmpresaSeeder;
use Database\Seeders\ProductoSeeder;
use Database\Seeders\VentaDetalleSeeder;
use Database\Seeders\ProveedorEmpresaSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
         $this->call(AlmacenSeeder::class);
         $this->call(ClienteSeeder::class);
         $this->call(CreditoSeeder::class);
         $this->call(EmpresaSeeder::class);
         $this->call(ProductoSeeder::class);
         $this->call(ProveedorEmpresaSeeder::class);
         $this->call(VentaDetalleSeeder::class);
    }
}
