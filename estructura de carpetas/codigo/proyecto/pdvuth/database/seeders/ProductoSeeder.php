<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $productos=[
            "1"=>[
                'id'=>1,
                'nombre'=>'Sabritas',
                'descripción'=>'Sabritas Ruffles 64gr',
                'precio'=>15.99,
                'codigo_barras'=>'7506473647563',
                'imagenes'=>'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi5.walmartimages.com%2Fasr%2Ffc43bb45-8934-4701-b118-a4314113e7d7_1.a14efb90d6c59540b6d4a868f00416c7.jpeg%3FodnHeight%3D450%26odnWidth%3D450%26odnBg%3Dffffff&f=1&nofb=1',
                'unidad_medida'=>'UNIDAD',
                'marca'=>'Ruffles',
                'categoria_id'=>1,
                'stock_minimo'=>40,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "2"=>[
                'id'=>2,
                'nombre'=>'Galletas Emperador',
                'descripción'=>'Galletas emperador chocolate 100gr',
                'precio'=>13.99,
                'codigo_barras'=>'750145215214',
                'imagenes'=>'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fklizmart6.webnode.es%2F_files%2Fsystem_preview_detail_200000013-0868309603%2Femperador%2520chocolate-800x800.jpg&f=1&nofb=1',
                'unidad_medida'=>'UNIDAD',
                'marca'=>'Emperador',
                'categoria_id'=>2,
                'stock_minimo'=>200,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "3"=>[
                'id'=>3,
                'nombre'=>'Soda Coca-Cola',
                'descripción'=>'Soda Coca-Cola 600ml sabor cola',
                'precio'=>16.00,
                'codigo_barras'=>'750145215214',
                'imagenes'=>'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fjumbocolombiafood.vteximg.com.br%2Farquivos%2Fids%2F188559-1000-1000%2F7702535001752COCA-COLA--CLASICA-X-600-ML.jpg%3Fv%3D636144861663370000&f=1&nofb=1',
                'unidad_medida'=>'UNIDAD',
                'marca'=>'Coca-Cola',
                'categoria_id'=>3,
                'stock_minimo'=>200,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "4"=>[
                'id'=>4,
                'nombre'=>'Empaque de Chaca-Chaca',
                'descripción'=>'Paquetito de trozos de Chaca-Chaca tamarindo 400gr',
                'precio'=>6.00,
                'codigo_barras'=>'750145215214',
                'imagenes'=>'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flacatrinacandystore.com%2Fwp-content%2Fuploads%2F2018%2F03%2FChaca-chaca-trozo-400gr..png&f=1&nofb=1',
                'unidad_medida'=>'UNIDAD',
                'marca'=>'Chaca-Chaca',
                'categoria_id'=>4,
                'stock_minimo'=>500,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "5"=>[
                'id'=>5,
                'nombre'=>'M&M amarillo',
                'descripción'=>'Chocolate M&M Amarillo con Cacahuate paquete de 6 piezas ',
                'precio'=>16.00,
                'codigo_barras'=>'750145215214',
                'imagenes'=>'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.laranitadelapaz.com.mx%2Fimages%2Fthumbs%2F0004733_chocolate-mms-amarillo-con-cacahuate-paquete-de-6-piezas-ieps-inc_510.jpeg&f=1&nofb=1',
                'unidad_medida'=>'UNIDAD',
                'marca'=>'M&M',
                'categoria_id'=>5,
                'stock_minimo'=>150,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
        ];

        foreach ($productos as $key => $producto) {
            # code...
            DB::table('productos')->insert($producto);
        }
    }
}
