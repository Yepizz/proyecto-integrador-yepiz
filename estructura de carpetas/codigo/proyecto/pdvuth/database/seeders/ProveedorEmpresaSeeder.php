<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProveedorEmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $provEmpresas=[
            "1"=>[
                'id'=>1,
                'empresa_id'=>1,
                'proveedor_id'=>1,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
            "2"=>[
                'id'=>2,
                'empresa_id'=>2,
                'proveedor_id'=>2,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
            "3"=>[
                'id'=>3,
                'empresa_id'=>3,
                'proveedor_id'=>3,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
            "4"=>[
                'id'=>4,
                'empresa_id'=>4,
                'proveedor_id'=>4,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
            "5"=>[
                'id'=>5,
                'empresa_id'=>5,
                'proveedor_id'=>5,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
        ];

        foreach ($provEmpresas as $key => $provEmpresa) {
            # code...
            DB::table('proveedores_empresa')->insert($provEmpresa);
        }
    }
}
