<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class VentaDetalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ventaDetalles=[
            "1"=>[
                'id'=>1,
                'venta_id'=>1,
                'cantidad'=>5,
                'precio'=>134.00,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "2"=>[
                'id'=>2,
                'venta_id'=>2,
                'cantidad'=>9,
                'precio'=>200.99,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "3"=>[
                'id'=>3,
                'venta_id'=>3,
                'cantidad'=>1,
                'precio'=>34.00,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "4"=>[
                'id'=>4,
                'venta_id'=>4,
                'cantidad'=>3,
                'precio'=>75.00,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            "5"=>[
                'id'=>5,
                'venta_id'=>5,
                'cantidad'=>5,
                'precio'=>125.00,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
        ];
    }
}
