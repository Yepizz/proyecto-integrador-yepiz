<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\HomeCustomer;
use App\Http\Livewire\AboutUs;
use App\Http\Livewire\AdminDashboard;
use App\Http\Livewire\AdminViewSales;
use App\Http\Livewire\AlmacenControl;
use App\Http\Livewire\Checkout;
use App\Http\Livewire\CrudCredits;
use App\Http\Livewire\CrudSupplier;
use App\Http\Livewire\DetailsProduct;
use App\Http\Livewire\OrderTracking;
use App\Http\Livewire\ProductCrud;
use App\Http\Livewire\Shop;
use App\Http\Livewire\ViewCart;
use App\Http\Livewire\ViewCredits;
use App\Http\Livewire\ViewProductRegistered;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// Ruta Home
Route::get('/home', HomeCustomer::class);
#Ruta About Us
Route::get('/about-us', AboutUs::class);
#Ruta Admin Home Dashboard
Route::get('/admin-home', AdminDashboard::class);
#Ruta Admin Sales
Route::get('/admin-sales', AdminViewSales::class);
#Ruta Almacén
Route::get('/warehouse', AlmacenControl::class);
#Ruta Checkout
Route::get('/checkout', Checkout::class);
#Ruta Creditos
Route::get('/credits', CrudCredits::class);
#Ruta Proveedores
Route::get('/suppliers', CrudSupplier::class);
#Ruta Detalles Productos
Route::get('/product-details', DetailsProduct::class);
#Ruta monitoreo Producto
Route::get('/order-tracking', OrderTracking::class);
#Ruta productos - Crud Productos
Route::get('/products', ProductCrud::class);
#Ruta Vista Productos a añadir carrito
Route::get('/shop', Shop::class);
#Ruta Carrito
Route::get('/cart', ViewCart::class);
#Ruta Créditos
Route::get('/credits', ViewCredits::class);
#Ruta Vista productos - Admin cuando accesa a todos los prods registrados
Route::get('/view-products', ViewProductRegistered::class);

